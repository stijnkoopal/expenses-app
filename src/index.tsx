import React from 'react';
import ReactDOM from 'react-dom';
import { setGlobal } from 'reactn';
import App from './App';

const sessionStorageCluster = window.sessionStorage.getItem('selectedMonetaryAccountCluster')
setGlobal({
  selectedMonetaryAccountCluster: sessionStorageCluster ? JSON.parse(sessionStorageCluster) : undefined
})

const root = document.getElementById('root') as HTMLElement
(ReactDOM as any).unstable_createRoot(root).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)
