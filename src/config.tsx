
const myHost = window.location.hostname
const myPortSublement = window.location.port
  ? `:${window.location.port}`
  : ''
export const myOrigin = `${window.location.protocol}//${myHost}${myPortSublement}`
export const apiOrigin = `${myOrigin}/api`
