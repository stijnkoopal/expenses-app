import { Box, CircularProgress, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core'
import AppBar from '@material-ui/core/AppBar'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Grid from '@material-ui/core/Grid'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { createStyles, makeStyles, Theme, useTheme } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import AccountBalanceWalletOutlinedIcon from '@material-ui/icons/AccountBalanceWalletOutlined'
import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined'
import MenuIcon from '@material-ui/icons/Menu'
import * as auth from 'auth'
import Can from 'auth/Can'
import useApolloClient from 'auth/use-apollo-client'
import gql from 'graphql-tag'
import React, { forwardRef, Suspense, useMemo, useState } from 'react'
import { ApolloProvider, useMutation } from 'react-apollo-hooks'
import { Link as RouterLink, LinkProps as RouterLinkProps } from 'react-router-dom'
import { useGlobal } from 'reactn'
import { MonetaryAccountCluster, useMonetaryAccountClusters } from 'scenes/Dashboard/use-monetary-account-clusters'
import UserMenu from './UserMenu'

const drawerWidth = 240

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      backgroundColor: theme.palette.background.paper,
      color: theme.palette.text.primary,
      boxShadow: theme.shadows[0],
      borderBottom: `1px solid ${theme.palette.divider}`,
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(1),
      [theme.breakpoints.up('sm')]: {
        padding: theme.spacing(2),
      },
      [theme.breakpoints.up('lg')]: {
        padding: theme.spacing(3),
      },
    },
    MonetaryAccountClusterSelector: {
      width: '200px',
    },
  })
)

interface ListItemLinkProps {
  icon?: React.ReactElement
  primary: string
  to: string
}

function ListItemLink(props: ListItemLinkProps) {
  const { icon, primary, to } = props

  const renderLink = useMemo(
    () =>
      forwardRef<any, Omit<RouterLinkProps, 'to'>>((itemProps, ref) => (
        <RouterLink to={to} ref={ref} {...itemProps} />
      )),
    [to]
  )

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  )
}

const Menu: React.FC = () => {
  const user = auth.getUser()
  return (
    <List>
      <ListItemLink
        icon={<DashboardOutlinedIcon />}
        primary="Dashboard"
        to="/dashboard"
      />
      <ListItemLink
        icon={<AccountBalanceWalletOutlinedIcon />}
        primary="Connections"
        to="/connections"
      />
      <Can
        permissions={(user && user.permissions) || []}
        perform="write:counterparty-categories"
        yes={() => (
          <ListItemLink primary="Counterparty categories (admin)" to="/admin" />
        )}
      />
    </List>
  )
}

interface SelectMonetaryAccountCluster {
  clusterId: string
}

const SELECT_MONETARY_ACCOUNT_CLUSTER_MUTATION = gql`
  mutation SelectMonetaryAccountCluster($clusterId: String!) {
    selectMonetaryAccountCluster(clusterId: $clusterId)
  }
`

const SuspendingMonetaryAccountClusterSelector: React.FC = () => {
  const monetaryAccountClusters = useMonetaryAccountClusters()

  const [
    selectedMonetaryAccountCluster,
    setMonetaryAccountCluster,
  ] = useGlobal<{ selectedMonetaryAccountCluster: MonetaryAccountCluster }>(
    'selectedMonetaryAccountCluster'
  )

  const [storeSelectionToBackend] = useMutation<SelectMonetaryAccountCluster>(
    SELECT_MONETARY_ACCOUNT_CLUSTER_MUTATION
  )

  const onChange = (id: string) => {
    const cluster = monetaryAccountClusters.find((cluster) => cluster.id === id)
    if (cluster) {
      setMonetaryAccountCluster(cluster)
      storeSelectionToBackend({ variables: { clusterId: cluster.id } })
    }
  }

  return (
    <MonetaryAccountClusterSelector
      values={monetaryAccountClusters}
      value={selectedMonetaryAccountCluster?.id}
      onMonetaryAccountClusterChange={onChange}
    />
  )
}

const HackProvideApolloClient: React.FC = ({ children }) => {
  const apolloClient = useApolloClient()
  return <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
}

const MonetaryAccountClusterSelector: React.FC<{
  values: { name: string; id: string }[]
  value?: string
  onMonetaryAccountClusterChange: (id: string) => void
}> = ({ value, values, onMonetaryAccountClusterChange }) => {
  const classes = useStyles()
  const [startTransition, isPending] = (React as any).unstable_useTransition({
    timeoutMs: 3000,
  })

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    startTransition(() => {
      onMonetaryAccountClusterChange(event.target.value as string)
    })
  }

  return (
    <Box display={'flex'} alignItems="center">
      <FormControl
        variant="outlined"
        className={classes.MonetaryAccountClusterSelector}
      >
        <InputLabel id="cluster-select-outlined-label">Cluster</InputLabel>
        <Select
          labelId="cluster-select-outlined-label"
          id="cluster-select-outlined"
          value={value || ''}
          onChange={handleChange}
          label="Cluster"
        >
          {values.map((cluster) => (
            <MenuItem key={cluster.id} value={cluster.id}>
              {cluster.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      {isPending && <CircularProgress size={20} />}
    </Box>
  )
}

const DrawerLayout: React.FC = ({ children }) => {
  const classes = useStyles()
  const theme = useTheme()
  const [mobileOpen, setMobileOpen] = useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const drawer = (
    <Suspense fallback={'loading...'}>
      <div>
        <div className={classes.toolbar} />
        <Divider />
        <Menu />
        <Divider />
      </div>
    </Suspense>
  )

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>

          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Suspense fallback={'loading...'}>
              <HackProvideApolloClient>
                <SuspendingMonetaryAccountClusterSelector />
              </HackProvideApolloClient>
            </Suspense>
            <div>
              <UserMenu />
            </div>
          </Grid>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  )
}

export default DrawerLayout
