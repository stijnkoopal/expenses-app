import React, { useCallback, Suspense, useState } from 'react'
import Button from '@material-ui/core/Button'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { Avatar, ListItemIcon } from '@material-ui/core'
import { AccountBox, ExitToApp } from '@material-ui/icons'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import * as auth from 'auth'
import LoaderAvatar from 'components/LoaderAvatar'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      width: theme.spacing(4),
      height: theme.spacing(4),
    },
  })
)

const OpenButton: React.FC<{ click: (e: any) => void }> = ({ click }) => {
  const classes = useStyles()
  const user = auth.getUser()

  return (
    <Button
      aria-controls="user-menu"
      aria-haspopup="true"
      onClick={click}
      startIcon={
        <Avatar
          alt={user?.name || ''}
          className={classes.avatar}
          src={user?.picture}
        />
      }
    >
      {user?.givenName || ''}
    </Button>
  )
}

const LoaderButton: React.FC = () => {
  return (
    <div style={{width: '87px', height: '44px'}}>
      <LoaderAvatar />
    </div>
  )
}

const UserMenu: React.FC = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

  const handleOpen = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) =>
      setAnchorEl(event.currentTarget),
    []
  )

  const onMenuClose = useCallback(() => setAnchorEl(null), [])

  return (
    <>
      <Suspense fallback={<LoaderButton />}>
        <OpenButton click={handleOpen} />
      </Suspense>

      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={onMenuClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={onMenuClose}>
          <ListItemIcon>
            <AccountBox fontSize="small" />
          </ListItemIcon>
          Profile
        </MenuItem>
        <MenuItem onClick={() => auth.logout()}>
          <ListItemIcon>
            <ExitToApp fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </>
  )
}

export default UserMenu
