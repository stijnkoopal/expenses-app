import React from 'react'
// import {
//   makeStyles,
//   useTheme,
//   Theme,
//   createStyles,
// } from '@material-ui/core/styles'

// const useStyles = makeStyles((theme: Theme) =>
//   createStyles({})
// )

const LandingLayout: React.FC = ({ children }) => {
  // const classes = useStyles()
  // const theme = useTheme()
  
  return (
    <>
      {children}
    </>
  )
}

export default LandingLayout
