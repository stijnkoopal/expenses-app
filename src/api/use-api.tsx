import { apiOrigin } from '../config'
import * as auth from 'auth'

export default function useApi() {
  const token = auth.token.read()
  const authorization = `Bearer ${token}`

  return {
    fetch: (url: string, init?: RequestInit) =>
      fetch(`${apiOrigin}${url}`, {
        ...init,
        headers: {
          ...(init || {}).headers,
          'CSRF-Token': 'nocheck',
          authorization: authorization as any,
        },
      }),
  }
}
