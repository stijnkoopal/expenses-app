import { OperationVariables } from 'apollo-boost'
import useApolloClient from 'auth/use-apollo-client'
import { DocumentNode } from 'graphql'
import { QueryHookOptions, useQuery } from 'react-apollo-hooks'

function useSuspendedQuery<
  TData = any,
  TVariables = OperationVariables,
  TCache = object
>(query: DocumentNode, options: QueryHookOptions<TVariables, TCache> = {}) {
  const apolloClient = useApolloClient()

  return useQuery<TData>(query, {
    ...options,
    suspend: true,
    client: apolloClient,
  })
}

export default useSuspendedQuery
