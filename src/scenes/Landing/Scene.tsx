import React from 'react'
import LandingLayout from 'layout/LandingLayout'
import * as auth from 'auth'

const Scene: React.FC = () => {
  return (
    <LandingLayout>
      <button onClick={() => auth.login()}>Log in</button>
    </LandingLayout>
  )
}

export default Scene
