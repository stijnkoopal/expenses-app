import React, { useEffect, Suspense } from 'react'
import DrawerLayout from 'layout/DrawerLayout'
import { useLocation, useParams } from 'react-router-dom'
import useApi from 'api/use-api'
import LoaderTable from 'components/LoaderTable'

const Authorize: React.FC<{connector?: string}> = ({connector}) => {
  const { fetch } = useApi()
  
  const query = useLocation().search

  useEffect(() => {
    fetch(`/${connector}/connect/authorize${query}`)
  })

  return (
    <div>{connector}</div>
  )
}

const Scene: React.FC = () => {
  const { connector } = useParams()
  

  return (
    <DrawerLayout>
      <Suspense fallback={<LoaderTable />}>
        <Authorize connector={connector} />
      </Suspense>
    </DrawerLayout>
  )
}

export default Scene
