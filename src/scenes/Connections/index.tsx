import Scene from './Scene'
import Authorize from './scenes/Authorize'

export default {
  Authorize,
  Scene,
}
