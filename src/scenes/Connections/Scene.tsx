import React, { Suspense } from 'react'
import DrawerLayout from 'layout/DrawerLayout'
import useApi from 'api/use-api'
import { Button } from '@material-ui/core'

const useBunqRedirect = () => {
  const { fetch } = useApi()

  return async () => {
    const response = await fetch('/bunq/connect', {
      method: 'POST',
      redirect: 'manual',
    })

    const bunqLocation = response.headers.get('Location')
    if (bunqLocation) {
      window.location.href = bunqLocation
    } else {
      alert('something went wrong')
    }
  }
}

const LinkBunqButton: React.FC<{onClick: () => void}> = ({onClick}) => {
  return (
    <Button variant="contained" color="primary" onClick={onClick}>
      Link bunq
    </Button>
  )
}

const LinkBunqButtonReal: React.FC = () => {
  const redirectToBunq = useBunqRedirect()

  return (
    <LinkBunqButton onClick={redirectToBunq} />
  )
}

const Scene: React.FC = () => {
  

  return (
    <DrawerLayout>
      <Suspense fallback={<LinkBunqButton onClick={() => undefined} />}>
        <LinkBunqButtonReal />
      </Suspense>
    </DrawerLayout>
  )
}

export default Scene
