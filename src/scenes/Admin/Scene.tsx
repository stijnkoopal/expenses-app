import React from 'react'
import { Grid } from '@material-ui/core'
import DrawerLayout from 'layout/DrawerLayout'
import AllocationCounterpartyCategoriesCard from './AllocationCounterpartyCategoriesCard';

const Scene: React.FC = () => {
  return (
    <DrawerLayout>
      <Grid container spacing={2}>
        <Grid item xs={12} md={12}>
          <AllocationCounterpartyCategoriesCard></AllocationCounterpartyCategoriesCard>
        </Grid>
      </Grid>
    </DrawerLayout>
  )
}

export default Scene
