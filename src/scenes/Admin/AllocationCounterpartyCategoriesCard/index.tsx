import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core'
import useSuspendedQuery from 'api/use-suspended-query'
import LoaderTable from 'components/LoaderTable'
import gql from 'graphql-tag'
import React, { Suspense, useState } from 'react'
import CategorizeAllocationCounterpartyDialog from './CategorizeAllocationCounterpartyDialog'

export interface AllocationCounterparty {
  id?: string
  name: string
  iban: string
  categoriesWhenPayee?: string[]
  categoriesWhenPayer?: string[]
  partialMatchOn?: string
  updated: Date
}

interface AllocationCounterpartiesResponse {
  allocationCounterparties: AllocationCounterparty[]
}

const ALLOCATION_COUNTERPARTIES_QUERY = gql`
  query AllocationCounterparties {
    allocationCounterparties {
      id
      name
      iban
      categoriesWhenPayee
      categoriesWhenPayer
      partialMatchOn
      updated
    }
  }
`

export interface TransactionCategory {
  id: string
  parent?: TransactionCategory
  label: string
  icon: string
}

interface TransactionCategoriesResponse {
  transactionCategories: {
    inbound: TransactionCategory[],
    outbound: TransactionCategory[]
  }
}

const TRANSACTION_CATEGORIES_QUERY = gql`
  query TransactionCategories {
    transactionCategories {
      inbound {
        id
        parent {
          id
          label
          icon
        }
        label
        icon
      }
      outbound {
        id
        parent {
          id
          label
          icon
        }
        label
        icon
      }
    }
  }
`

function getHighlightedText(text: string, highlight: string) {
  // Split on highlight term and include term into parts, ignore case
  const parts = text.split(new RegExp(`(${highlight})`, 'gi'))
  return (
    <span>
      {' '}
      {parts.map((part, i) => (
        <span
          key={i}
          style={
            part.toLowerCase() === highlight.toLowerCase()
              ? { fontWeight: 'bold' }
              : {}
          }
        >
          {part}
        </span>
      ))}{' '}
    </span>
  )
}

const AllocationCounterpartiesTable: React.FC<{
  counterparties: AllocationCounterparty[],
  onRowClick: (counterparty: AllocationCounterparty) => void
}> = ({ counterparties, onRowClick }) => {
  return (
    <TableContainer component={Paper}>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Iban</TableCell>
            <TableCell>Category when payer</TableCell>
            <TableCell>Category when payee</TableCell>
            <TableCell>Updated</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {counterparties.map(counterparty => (
            <TableRow key={counterparty.id} onClick={() => onRowClick(counterparty)}>
              <TableCell>
                {counterparty.partialMatchOn
                  ? getHighlightedText(
                      counterparty.name,
                      counterparty.partialMatchOn
                    )
                  : counterparty.name}
              </TableCell>
              <TableCell>{counterparty.iban}</TableCell>
              <TableCell>{counterparty.categoriesWhenPayer}</TableCell>
              <TableCell>{counterparty.categoriesWhenPayee}</TableCell>
              <TableCell>{counterparty.updated}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

const AllocationCounterpartyCategoriesContainer: React.FC = () => {
  const { data: allocationCounterpartiesResponse } = useSuspendedQuery<
    AllocationCounterpartiesResponse
  >(ALLOCATION_COUNTERPARTIES_QUERY)

  const { data: transactionCategoriesResponse } = useSuspendedQuery<
    TransactionCategoriesResponse
  >(TRANSACTION_CATEGORIES_QUERY)

  const [selectedCounterparty, selectCounterparty] = useState<AllocationCounterparty | undefined>(undefined)

  if (!transactionCategoriesResponse || !allocationCounterpartiesResponse) {
    return <div>Error!</div>
  }

  return (
    <>
      <CategorizeAllocationCounterpartyDialog
        onClose={() => selectCounterparty(undefined)}
        onSave={c => console.log(c)} // TODO
        counterparty={selectedCounterparty}
        inboundCategories={transactionCategoriesResponse.transactionCategories.inbound || []}
        outboundCategories={transactionCategoriesResponse.transactionCategories.outbound || []}
      />
      <AllocationCounterpartiesTable
        counterparties={
          allocationCounterpartiesResponse.allocationCounterparties || []
        }
        onRowClick={c => selectCounterparty(c)}
      />
    </>
  )
}

const AllocationCounterpartyCategoriesCard: React.FC = () => {
  return (
    <Suspense fallback={<LoaderTable />}>
      <AllocationCounterpartyCategoriesContainer />
    </Suspense>
  )
}

export default AllocationCounterpartyCategoriesCard
