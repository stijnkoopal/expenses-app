import React, { useState } from 'react'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  FormControl,
  InputLabel,
  Select,
  Input,
  TextField,
} from '@material-ui/core'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import { AllocationCounterparty, TransactionCategory } from '.'
import DescriptionIcon from '@material-ui/icons/Description'
import AccountBalanceIcon from '@material-ui/icons/AccountBalance'
import CategoryIcon from '@material-ui/icons/Category'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
  })
)

interface TransactionCategoryGroup {
  parent: TransactionCategory
  items: TransactionCategory[]
}

export interface AllocationCounterpartyUpdate {
  counterpartyId: string
  categoryWhenPayer?: string
  categoryWhenPayee?: string
  partialMatchOn?: string
}

const grouped = (
  categories: TransactionCategory[]
): TransactionCategoryGroup[] => {
  const groups: Map<string, TransactionCategoryGroup> = new Map(
    categories
      .filter(category => !category.parent)
      .map(category => [category.id, { parent: category, items: [] }])
  )

  categories
    .filter(category => category.parent)
    .forEach(category => groups.get(category.parent!.id)?.items.push(category))

  return Array.from(groups.values())
}

const CategorySelect: React.FC<{
  label: string
  categories: TransactionCategory[]
  className?: string
  onChange: (category: string | undefined) => void
}> = ({ label, categories, onChange, className = '' }) => {
  const groupedCategories = grouped(categories)

  return (
    <FormControl className={className}>
      <InputLabel shrink htmlFor="category-incoming-select">
        {label}
      </InputLabel>
      <Select
        native
        defaultValue=""
        input={<Input id="category-incoming-select" />}
        onChange={e => onChange(e.target.value as string)}
      >
        <option value={undefined} key="None">
          None
        </option>
        {groupedCategories.map(group => {
          if (group.items.length > 0) {
            return (
              <optgroup key={group.parent.id} label={group.parent.label}>
                {group.items.map(category => (
                  <option value={category.id} key={category.id}>
                    {category.label}
                  </option>
                ))}
              </optgroup>
            )
          } else {
            return (
              <option value={group.parent.id} key={group.parent.id}>
                {group.parent.label}
              </option>
            )
          }
        })}
      </Select>
    </FormControl>
  )
}
const CategorizeAllocationCounterpartyDialog: React.FC<{
  counterparty?: AllocationCounterparty
  inboundCategories: TransactionCategory[]
  outboundCategories: TransactionCategory[]
  onSave: (c: AllocationCounterpartyUpdate) => void
  onClose: () => void
}> = ({ onClose, onSave, counterparty, inboundCategories, outboundCategories }) => {
  const classes = useStyles()

  const [update, setUpdate] =
    useState <
    AllocationCounterpartyUpdate>({
      counterpartyId: counterparty?.id || '',
      partialMatchOn: counterparty?.partialMatchOn,
      categoryWhenPayer:
        (counterparty?.categoriesWhenPayer?.length || 0) > 0
          ? counterparty!.categoriesWhenPayer![0]
          : undefined,
      categoryWhenPayee:
        (counterparty?.categoriesWhenPayee?.length || 0) > 0
          ? counterparty!.categoriesWhenPayee![0]
          : undefined,
    })

  return (
    <Dialog
      fullWidth={true}
      maxWidth="md"
      open={counterparty !== undefined}
      aria-labelledby="categorize-allocation-counterparty-dialog-title"
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
    >
      <DialogTitle id="categorize-allocation-counterparty-dialog-title">
        {`Label ${counterparty?.name}`}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <List className={classes.root}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <DescriptionIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={counterparty?.name} />
            </ListItem>
            {counterparty?.iban && (
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <AccountBalanceIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={counterparty?.iban} />
              </ListItem>
            )}
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <CategoryIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={
                  <CategorySelect
                    label={'Category when incoming'}
                    onChange={value =>
                      setUpdate({ ...update, categoryWhenPayee: value })
                    }
                    categories={inboundCategories}
                    className={classes.root}
                  />
                }
              />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <CategoryIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={
                  <CategorySelect
                    className={classes.root}
                    onChange={value =>
                      setUpdate({ ...update, categoryWhenPayer: value })
                    }
                    label={'Category when outgoing'}
                    categories={outboundCategories}
                  />
                }
              />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <CategoryIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={
                  <TextField
                    id="partial-match-on"
                    label="Partial match on"
                    defaultValue={counterparty?.name}
                    onChange={e =>
                      setUpdate({
                        ...update,
                        partialMatchOn:
                          e.target.value === counterparty?.name
                            ? undefined
                            : e.target.value,
                      })
                    }
                    className={classes.root}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                }
              />
            </ListItem>
          </List>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Close
        </Button>
        <Button onClick={() => onSave({...update, counterpartyId: counterparty?.id || ''})} autoFocus color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default CategorizeAllocationCounterpartyDialog
