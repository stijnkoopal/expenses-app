import React from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import TrendingFlatIcon from '@material-ui/icons/TrendingFlat'
import { green, red } from '@material-ui/core/colors'
import Iban from '../../components/Iban'
import { Chip, Button } from '@material-ui/core'
import CategoryIcon from '@material-ui/icons/Category'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
    reverse: {
      transform: 'rotate(180deg)'
    },
    green: {
      border: `1px solid ${green[300]}`,
    },
    red: {
      border: `1px solid ${red[300]}`,
    },
  })
)

const AllocationCounterpartyCard = () => {
  const classes = useStyles()

  return (
    <List className={classes.root}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar>
            <AccountCircleIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="Stijn Koopal"
          secondary={<Iban iban={'NL04ABNA0590505351'} />}
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar>
            <CategoryIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={
            <Chip
              className={classes.green}
              variant="outlined"
              icon={<TrendingFlatIcon />}
              label="Groceries"
            />
          }
          secondary={
            <Chip
              className={classes.red}
              variant="outlined"
              icon={<TrendingFlatIcon className={classes.reverse} />}
              label="Income"
            />
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
          <ListItemAvatar>
              <></>
          </ListItemAvatar>
        <ListItemText>
          <Button variant="outlined" color="primary">
            Categorize
          </Button>
        </ListItemText>
      </ListItem>
    </List>
  )
}

export default AllocationCounterpartyCard
