import { Grid } from '@material-ui/core'
import DrawerLayout from 'layout/DrawerLayout'
import React from 'react'
import AllocationsCard from './AllocationsCard'
import HistoricalNetWorthCard from './HistoricalNetWorthCard'
import MonetaryAccountsCard from './MonetaryAccountsCard'
import MonthlyAveragesCard from './MonthlyAveragesCard'
import MonthlyCashflowCard from './MonthlyCashflowCard'
import RecurringCard from './RecurringCard'
import SpendingLeftThisMonthCard from './SpendingLeftThisMonthCard'

const Scene: React.FC = () => {
  return (
    <DrawerLayout>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} lg={3}>
          <SpendingLeftThisMonthCard />
        </Grid>
        <Grid item xs={12} md={6}>
          <MonthlyAveragesCard />
        </Grid>
        <Grid item xs={12}>
          <MonthlyCashflowCard />
        </Grid>
        <Grid item xs={12}>
          <MonetaryAccountsCard />
        </Grid>
        <Grid item xs={12}>
          <RecurringCard />
        </Grid>
        <Grid item xs={12}>
          <AllocationsCard />
        </Grid>
        <Grid item xs={12}>
          <HistoricalNetWorthCard />
        </Grid>
      </Grid>
    </DrawerLayout>
  )
}

export default Scene
