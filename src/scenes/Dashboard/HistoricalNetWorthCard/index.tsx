import React from 'react'
import { CardContent, Card, CardHeader, Paper } from '@material-ui/core'
import HistoricalNetWorthContainer from './HistoricalNetWorthContainer'

const HistoricalNetWorthCard: React.FC = () => {
  return (
    <Paper>
      <Card variant="outlined">
        <CardHeader title="Historical networth" />
        <CardContent>
          <HistoricalNetWorthContainer />
        </CardContent>
      </Card>
    </Paper>
  )
}

export default HistoricalNetWorthCard
