import useSuspendedQuery from 'api/use-suspended-query'
import { addMonths, endOfMonth, format, parseISO, startOfMonth, subMonths } from 'date-fns'
import gql from 'graphql-tag'
import React, { Suspense } from 'react'
import LoaderXYChart from '../../../components/LoaderXYChart'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'
import MonthlyNetWorthChart, { MonthlyNetWorthDataPoint } from './MonthlyNetWorthChart'

interface MonthlyBalance {
  monthStart: string
  monetaryAccount: {
    alias: string,
    joint: boolean
  }
  balance?: number
}

interface MonthlyBalancesResponse {
  monthlyBalances: MonthlyBalance[]
}

const MONTHLY_BALANCES = gql`
  query MonthlyBalances($monetaryAccounts: [MonetaryAccountConfiguration!]!, $startDate: LocalDate!, $endDate: LocalDate!) {
    monthlyBalances(monetaryAccounts: $monetaryAccounts, startDate: $startDate, endDate: $endDate) {
      monthStart
      monetaryAccount {
        alias,
        joint
      }
      balance
    }
  }
`

const prepareBalancesData = (
  aliases: string[],
  data: MonthlyBalance[]
): MonthlyNetWorthDataPoint[] => {
  const result = new Map<number, Map<string, number | undefined>>()
  data.forEach(point => {
    const monthStart = startOfMonth(parseISO(point.monthStart)).getTime()

    if (!result.has(monthStart)) {
      result.set(monthStart, new Map())
    }

    result.set(monthStart, result.get(monthStart)!.set(point.monetaryAccount.alias, point.balance))
  })

  return Array.from(result.keys()).map(monthStart => ({
    month: new Date(monthStart),
    ...Object.fromEntries(result.get(monthStart) || new Map())
  }))
}

const HistoricalNetWorth: React.FC<{
  periodStart: Date
  periodEnd: Date
}> = ({ periodStart, periodEnd }) => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()
  const { data, error } = useSuspendedQuery<MonthlyBalancesResponse>(
    MONTHLY_BALANCES,
    {
      variables: {
        startDate: format(periodStart, 'yyyy-MM-dd'),
        endDate: format(periodEnd, 'yyyy-MM-dd'),
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  const aliases: string[] = Array.from(
    new Set(data.monthlyBalances.map(({ monetaryAccount: { alias } }) => alias))
  )

  return (
    <MonthlyNetWorthChart
      data={prepareBalancesData(aliases, data.monthlyBalances)}
      stacks={aliases}
      periodStart={periodStart}
      periodEnd={periodEnd}
    ></MonthlyNetWorthChart>
  )
}

const HistoricalNetWorthContainer: React.FC = () => {
  const periodMonths = 36
  const periodStart = subMonths(startOfMonth(new Date()), periodMonths)
  const periodEnd = endOfMonth(addMonths(periodStart, periodMonths))

  return (
    <Suspense fallback={<LoaderXYChart />}>
      <HistoricalNetWorth periodStart={periodStart} periodEnd={periodEnd} />
    </Suspense>
  )
}

export default HistoricalNetWorthContainer
