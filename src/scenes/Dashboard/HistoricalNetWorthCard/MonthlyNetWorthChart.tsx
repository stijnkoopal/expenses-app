import { List, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import { ResponsiveBar, Value } from '@nivo/bar'
import NivoChartsContainer from 'components/NivoChartsContainer'
import { addMonths, format, getMonth, isAfter } from 'date-fns'
import React from 'react'
import { formatCurrency } from 'scenes/format-currency'

export interface MonthlyNetWorthDataPoint {
  month: Date
  [alias: string]: number | Date | undefined
}

const findMonthsBetween = (dateStart: Date, dateEnd: Date): Date[] => {
  const monthsInBetween: Date[] = []
  let dateChange = dateStart

  while (
    isAfter(dateEnd, dateChange) ||
    getMonth(dateChange) === getMonth(dateEnd)
  ) {
    monthsInBetween.push(dateChange)
    dateChange = addMonths(dateChange, 1)
  }

  return monthsInBetween
}

const appendEmptyMonths = (
  data: MonthlyNetWorthDataPoint[],
  dateStart: Date,
  dateEnd: Date
): MonthlyNetWorthDataPoint[] => {
  if (isAfter(dateStart, dateEnd)) {
    return data
  }

  const monthsInBetween = findMonthsBetween(dateStart, dateEnd)

  const castedData = data.map((d) => ({ ...d, month: d.month.getTime() }))
  const result: MonthlyNetWorthDataPoint[] = [...data]
  monthsInBetween.forEach((month) => {
    if (!castedData.map(({ month }) => month).includes(month.getTime())) {
      result.push({
        month,
      })
    }
  })

  return result.sort((a, b) => (isAfter(a.month, b.month) ? 1 : -1))
}

const extrapolateGaps = (
  data: MonthlyNetWorthDataPoint[]
): MonthlyNetWorthDataPoint[] => {
  const result = data
  for (let i = 1; i < data.length; i++) {
    const keysInPreviousMonth = Object.keys(data[i - 1])
    const keysInCurrentMonth = new Set(Object.keys(data[i]))

    const differenceInKeys = new Set(
      keysInPreviousMonth.filter(
        (x) => !keysInCurrentMonth.has(x) || data[i][x] === undefined
      )
    )

    if (differenceInKeys.size) {
      const gap = keysInPreviousMonth.reduce(
        (acc, val) =>
          differenceInKeys.has(val) ? { ...acc, [val]: data[i - 1][val] } : acc,
        { month: data[i].month }
      )
      result[i] = { ...data[i], ...gap }
    }
  }

  return result
}

// TODO: Extrapolated data should be visible some how
// TODO: add reference line for years

const adaptToNivoSeries = (data: MonthlyNetWorthDataPoint[]): object[] => {
  return data.map((point) => ({
    ...point,
    month: format(point.month, 'y-MM-dd'),
  }))
}

const Tooltip: React.FC<{
  hoverId: Value
  financials: { [key: string]: Value }
}> = ({ hoverId, financials }) => {
  const sortedKeys = Object.keys(financials)
    .filter((key) => key !== 'month')
    .sort((a, b) =>
      a === hoverId ? -1 : b === hoverId ? 1 : a.localeCompare(b)
    )

  return (
    <>
      <strong>{financials.month}</strong>
      <List dense>
        <ListItem key={'sum'}>
          <ListItemText primary={<strong>Sum</strong>} />
          <ListItemSecondaryAction>
            <strong>
              {formatCurrency(
                sortedKeys.reduce(
                  (acc, val) => acc + (financials[val] as any),
                  0
                )
              )}
            </strong>
          </ListItemSecondaryAction>
        </ListItem>
        {sortedKeys.map((key) => (
          <ListItem key={key} button>
            <ListItemText primary={key} />
            <ListItemSecondaryAction>
              {formatCurrency(financials[key] as any)}
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </>
  )
}

const MonthlyNetWorthChart: React.FC<{
  data: MonthlyNetWorthDataPoint[]
  stacks: string[]
  periodStart: Date
  periodEnd: Date
}> = ({ data, stacks, periodStart, periodEnd }) => {
  const sortedData: MonthlyNetWorthDataPoint[] = data.sort((a, b) =>
    a.month < b.month ? -1 : 1
  )

  const correctedData: object[] = adaptToNivoSeries(
    extrapolateGaps(appendEmptyMonths(sortedData, periodStart, periodEnd))
  )

  return (
    <NivoChartsContainer>
      <ResponsiveBar
        data={correctedData}
        keys={stacks}
        indexBy="month"
        margin={{ top: 10, right: 130, bottom: 50, left: 60 }}
        padding={0.3}
        colors={{ scheme: 'nivo' }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 90,
          // tickValues: ['2020-01-01', '2020-04-01']
        }}
        markers={[
          { axis: 'x', value: '2020-01-01' },
          { axis: 'x', value: '2019-01-01' },
          { axis: 'x', value: '2018-01-01' },
        ]}
        axisLeft={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          format: (value) => formatCurrency(Number(value)),
        }}
        tooltipFormat={(value) => formatCurrency(Number(value))}
        tooltip={({ id, data }) => <Tooltip hoverId={id} financials={data} />}
        legends={[
          {
            dataFrom: 'keys',
            anchor: 'bottom-right',
            direction: 'column',
            justify: false,
            translateX: 120,
            translateY: 0,
            itemsSpacing: 2,
            itemWidth: 100,
            itemHeight: 20,
            itemDirection: 'left-to-right',
            itemOpacity: 0.85,
            symbolSize: 20,
            effects: [
              {
                on: 'hover',
                style: {
                  itemOpacity: 1,
                },
              },
            ],
          },
        ]}
        animate={false}
        enableLabel={false}
      />
    </NivoChartsContainer>
  )
}

export default MonthlyNetWorthChart
