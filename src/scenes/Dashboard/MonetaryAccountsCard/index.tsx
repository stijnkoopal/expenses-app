import { Button, Card, CardContent, CardHeader, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, List, ListItem, Menu, MenuItem, Paper, TextField } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import useSuspendedQuery from 'api/use-suspended-query'
import useApolloClient from 'auth/use-apollo-client'
import LoaderMenuItem from 'components/LoaderMenuItem'
import LoaderTable from 'components/LoaderTable'
import gql from 'graphql-tag'
import React, { Suspense, useCallback, useState } from 'react'
import { ApolloProvider, useMutation } from 'react-apollo-hooks'
import useMenuAnchor from '../use-menu-anchor'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'
import MonetaryAcountsContainer from './MonetaryAccountsContainer'

interface BalanceSnapshot {
  monetaryAccountId: string
  balance: number
}

interface BalancesSnapshotBatch {
  balances: BalanceSnapshot[]
}

const PROVIDE_BALANCES_SNAPSHOT_BATCH = gql`
  mutation ProvideBalanceSnapshotBatch(
    $balanceSnapshotBatch: BalanceSnapshotBatch!
  ) {
    provideBalanceSnapshotBatch(balanceSnapshotBatch: $balanceSnapshotBatch)
  }
`

interface MonetaryAccount {
  id: string
  iban: string
  alias: string
  latestBalance: {
    balance: number
    latestUpdate: Date
  }
  autoUpdated: boolean
}

interface MonetaryAccountsResponse {
  monetaryAccounts: MonetaryAccount[]
}

const MONETARY_ACCOUNTS_QUERY = gql`
  query MonetaryAccounts($monetaryAccounts: [MonetaryAccountConfiguration!]!) {
    monetaryAccounts(monetaryAccounts: $monetaryAccounts) {
      id
      iban
      alias
      latestBalance {
        balance
        latestUpdate
      }
      autoUpdated
      lastUpdated
    }
  }
`

type BalanceChangeFn = (id: string, balance: number) => void

const ManualAccountList: React.FC<{
  balances: Balances
  onBalanceChange: BalanceChangeFn
}> = ({ balances, onBalanceChange }) => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()
  const { data, error } = useSuspendedQuery<MonetaryAccountsResponse>(
    MONETARY_ACCOUNTS_QUERY,
    {
      variables: {
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  const setBalance = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      if (event.target.validity.valid)
        onBalanceChange(event.target.id, parseFloat(event.target.value) || 0)
    },
    [onBalanceChange]
  )

  if (!data || error) {
    return <div>Error!</div>
  }

  const manualAcounts = data.monetaryAccounts.filter(
    account => !account.autoUpdated
  )

  return (
    <form>
      <List>
        {manualAcounts.map(account => (
          <ListItem alignItems="flex-start" key={account.id}>
            <TextField
              id={account.id}
              fullWidth
              label={`${account.alias} ${
                account.iban ? '(' + account.iban + ')' : ''
              }`}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              helperText={`Last balance: ${account.latestBalance.balance} (${account.latestBalance.latestUpdate})`}
              value={balances[account.id]}
              onChange={setBalance}
            />
          </ListItem>
        ))}
      </List>
    </form>
  )
}

type Balances = { [accountId: string]: number }
type OnSaveFn = (balances: Balances) => void

const SetManualBalancesDialog: React.FC<{
  open: boolean
  onCancel: any
  onSave: OnSaveFn
}> = ({ open, onCancel, onSave }) => {
  const [balances, setBalances] = useState<Balances>({})

  const handleBalanceChange = useCallback(
    (accountId: string, balance: number) =>
      setBalances({ ...balances, [accountId]: balance }),
      // TODO: save in accounts
    [balances]
  )

  return (
    <Dialog
      fullWidth={true}
      maxWidth="md"
      open={open}
      onClose={onCancel}
      aria-labelledby="manual-balances-dialog-title"
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
    >
      <DialogTitle id="manual-balances-dialog-title">
        {'Set balances to manually tracked accounts'}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Suspense fallback={<LoaderTable />}>
            <ManualAccountList
              balances={balances}
              onBalanceChange={handleBalanceChange}
            />
          </Suspense>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="secondary">
          Cancel
        </Button>
        <Button autoFocus onClick={() => onSave(balances)} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const ManualBalancesMenuItem: React.FC<{ onClick: () => void }> = ({
  onClick,
}) => {
  const [open, setOpen] = useState(false)

  const handleModalOpen = useCallback(() => {
    setOpen(true)
    onClick()
  }, [onClick])

  const [provideBalance] = useMutation<BalancesSnapshotBatch>(
    PROVIDE_BALANCES_SNAPSHOT_BATCH
  )

  const handleSave = useCallback(
    (balances: Balances) => {
      if (Object.keys(balances).length) {
        const data = Object.keys(balances).map(id => ({
          monetaryAccount: id,
          balance: balances[id],
        }))

        provideBalance({
          variables: {
            balanceSnapshotBatch: {
              balances: data,
            },
          },
        })
      }
      setOpen(false)
    },
    [provideBalance]
  )

  const handleCancel = useCallback(() => setOpen(false), [])

  return (
    <>
      <MenuItem onClick={handleModalOpen}>Set manual balances</MenuItem>
      <SetManualBalancesDialog
        open={open}
        onCancel={handleCancel}
        onSave={handleSave}
      />
    </>
  )
}

const HackProvideApolloClient: React.FC = ({children}) => {
  const apolloClient = useApolloClient()
  return (
    <ApolloProvider client={apolloClient}>
      {children}
    </ApolloProvider>
  )
}

const MonetaryAccountsCard: React.FC = () => {
  const [anchor, handleMenuOpen, handleMenuClose] = useMenuAnchor()

  return (
    <Paper>
      <Card variant="outlined">
        <CardHeader
          title="Monetary accounts"
          action={
            <IconButton
              aria-label="settings"
              onClick={handleMenuOpen}
              aria-controls="monetary-accounts-more-menu"
              aria-haspopup="true"
            >
              <MoreVertIcon />
            </IconButton>
          }
        />
        <Menu
          id="monetary-accounts-more-menu"
          anchorEl={anchor}
          keepMounted
          open={Boolean(anchor)}
          onClose={handleMenuClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          getContentAnchorEl={null}
        >
          <Suspense fallback={<LoaderMenuItem />}>
            <HackProvideApolloClient>
              <ManualBalancesMenuItem onClick={handleMenuClose} />
            </HackProvideApolloClient>
          </Suspense>
        </Menu>
        <CardContent>
          <MonetaryAcountsContainer />
        </CardContent>
      </Card>
    </Paper>
  )
}

export default MonetaryAccountsCard
