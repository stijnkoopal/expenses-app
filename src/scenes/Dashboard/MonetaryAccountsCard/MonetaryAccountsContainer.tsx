import useSuspendedQuery from 'api/use-suspended-query'
import { parseISO } from 'date-fns'
import gql from 'graphql-tag'
import React, { Suspense } from 'react'
import LoaderTable from '../../../components/LoaderTable'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'
import MonetaryAccountsTable, { MonetaryAccount as TableMonetaryAccount } from './MonetaryAccountsTable'

interface MonetaryAccount {
  id: string
  iban: string
  alias: string
  latestBalance: {
    balance: number
    latestUpdate: string
  }
  autoUpdated: boolean
  institution: string
}

interface MonetaryAccountsResponse {
  monetaryAccounts: MonetaryAccount[]
}

const MONETARY_ACCOUNTS_QUERY = gql`
  query MonetaryAccounts($monetaryAccounts: [MonetaryAccountConfiguration!]!) {
    monetaryAccounts(monetaryAccounts: $monetaryAccounts) {
      id
      iban
      alias
      latestBalance {
        balance
        latestUpdate
      }
      autoUpdated
      institution
    }
  }
`

const perpareMonetaryAccountsData = (
  data: MonetaryAccount[]
): TableMonetaryAccount[] =>
  data.map((account) => ({
    iban: account.iban,
    name: account.alias,
    balance: account.latestBalance.balance,
    lastUpdate: parseISO(account.latestBalance.latestUpdate),
  }))

const MonetaryAccounts: React.FC = () => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()
  const { data, error } = useSuspendedQuery<MonetaryAccountsResponse>(
    MONETARY_ACCOUNTS_QUERY,
    {
      variables: {
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  return (
    <MonetaryAccountsTable
      data={perpareMonetaryAccountsData(data.monetaryAccounts)}
    />
  )
}

const MonetaryAcountsContainer: React.FC = () => {
  return (
    <Suspense fallback={<LoaderTable />}>
      <MonetaryAccounts />
    </Suspense>
  )
}

export default MonetaryAcountsContainer
