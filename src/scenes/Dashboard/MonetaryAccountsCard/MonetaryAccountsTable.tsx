import { Paper, TableContainer } from '@material-ui/core'
import { formatDistanceToNow } from 'date-fns'
import React from 'react'
import { Table, Tbody, Td, Th, Thead, Tr } from 'react-super-responsive-table'
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css'
import { formatCurrency } from 'scenes/format-currency'

export interface MonetaryAccount {
  iban: string
  name: string
  balance: number
  lastUpdate: Date
}

const MonetaryAccountsTable: React.FC<{ data: MonetaryAccount[] }> = ({
  data,
}) => (
  <TableContainer component={Paper}>
    <Table size="small" aria-label="a dense table">
      <Thead>
        <Tr>
          <Th>Name</Th>
          <Th>Iban</Th>
          <Th>Balance</Th>
          <Th>Last update</Th>
        </Tr>
      </Thead>
      <Tbody>
        {data.map(({ iban, name, balance, lastUpdate }) => (
          <Tr key={name}>
            <Td>{name}</Td>
            <Td>{iban}</Td>
            <Td>{formatCurrency(balance)}</Td>
            <Td>{formatDistanceToNow(lastUpdate)}</Td>
          </Tr>
        ))}
        <Tr>
          <Td colSpan={2}>Total</Td>
          <Td colSpan={2}>
            {formatCurrency(data.reduce((acc, val) => acc + val.balance, 0))}
          </Td>
        </Tr>
      </Tbody>
    </Table>
  </TableContainer>
)

export default MonetaryAccountsTable
