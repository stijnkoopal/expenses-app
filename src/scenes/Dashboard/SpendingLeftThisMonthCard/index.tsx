import { Box, Card, CardContent, CardHeader, Paper } from '@material-ui/core'
import useSuspendedQuery from 'api/use-suspended-query'
import { format, parseISO } from 'date-fns'
import gql from 'graphql-tag'
import React, { Suspense } from 'react'
import { formatCurrency } from 'scenes/format-currency'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'

interface SpendingLeftResponse {
  leftOverUntil: {
    leftOver: number
    from: string
    until: string
    outboundSoFar: number
    recurringOutbound: number
    projectedInbound: number
  }
}

const LEFT_OVER_THIS_MONTH_QUERY = gql`
  query LeftOverUntil($monetaryAccounts: [MonetaryAccountConfiguration!]!) {
    leftOverUntil(monetaryAccounts: $monetaryAccounts) {
      leftOver
      from
      until
      outboundSoFar
      recurringOutbound
      projectedInbound
    }
  }
`

const SpendingLeft: React.FC<{
  left: number
  projectedInbound: number
  recurringOutbound: number
  outboundSoFar: number
  until: Date
}> = ({
  left,
  until,
  projectedInbound,
  recurringOutbound,
  outboundSoFar,
}) => {
  return (
    <Box display="flex" flexDirection="row" justifyContent="space-between">
      <Box display="flex" flexDirection="column">
        <Box fontWeight="fontWeightBold" fontSize={'1.75rem'}>
          {formatCurrency(left)}
        </Box>
        <Box fontSize={'1 rem'}>Until {format(until, 'MMMM dd')}</Box>
      </Box>
      <Box display="flex" flexDirection="column">
        <Box>
          <Box>Avg. incoming</Box>
          <Box>{formatCurrency(projectedInbound)}</Box>
        </Box>

        <Box>
          <Box>Recurring outgoing</Box>
          <Box>{formatCurrency(recurringOutbound)}</Box>
        </Box>

        <Box>
          <Box>Outgoing so-far</Box>
          <Box>{formatCurrency(outboundSoFar)}</Box>
        </Box>
      </Box>
    </Box>
  )
}

const SpendingLeftSuspending: React.FC = () => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()

  const { data, error } = useSuspendedQuery<SpendingLeftResponse>(
    LEFT_OVER_THIS_MONTH_QUERY,
    {
      variables: {
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  const leftOverUntil = data.leftOverUntil

  return (
    <SpendingLeft
      left={leftOverUntil.leftOver}
      until={parseISO(leftOverUntil.until)}
      projectedInbound={leftOverUntil.projectedInbound}
      outboundSoFar={leftOverUntil.outboundSoFar}
      recurringOutbound={leftOverUntil.recurringOutbound}
    />
  )
}

const SpendingLeftThisMonthCard: React.FC = () => {
  return (
    <Paper>
      <Card variant="outlined">
        <CardHeader title="Spending left" />
        <CardContent>
          <Suspense fallback={'loading...'}>
            <SpendingLeftSuspending />
          </Suspense>
        </CardContent>
      </Card>
    </Paper>
  )
}

export default SpendingLeftThisMonthCard
