import { Paper, TableContainer } from '@material-ui/core'
import { format } from 'date-fns'
import React from 'react'
import { Table, Tbody, Td, Th, Thead, Tr } from 'react-super-responsive-table'
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css'
import { formatCurrency } from 'scenes/format-currency'

export enum TransactionDirection {
  Inbound = 'inbound',
  Outbound = 'outbound',
}

export interface Party {
  name?: string
  iban?: string
}

export interface RecurringTransaction {
  payee: Party
  payer: Party
  monetaryAccountId: string
  amount: number
  start: Date
  end?: Date
  frequency: string
}

export type RecurringDirection = TransactionDirection

const RecurringTransactionsTable: React.FC<{
  data: RecurringTransaction[]
  direction: RecurringDirection
}> = ({ data, direction }) => {
  return (
    <TableContainer component={Paper}>
      <Table size="small" aria-label={`recuring ${direction}`}>
        <Thead>
          <Tr>
            <Th>{direction === 'inbound' ? 'From' : 'To'}</Th>
            <Th>Amount</Th>
            <Th>Frequency</Th>
            <Th>Period</Th>
          </Tr>
        </Thead>
        <Tbody>
          {data.map(({ payee, payer, amount, start, end, frequency }) => (
            <Tr key={`${amount}-${start}`}>
              <Td>{direction === 'inbound' ? payer.name : payee.name}</Td>
              <Td>{formatCurrency(amount)}</Td>
              <Td>{frequency}</Td>
              <Td>
                {format(start, 'yyyy-MM-dd')} -{' '}
                {end ? format(end, 'yyyy-MM-dd') : 'now'}
              </Td>
            </Tr>
          ))}
          <Tr>
            <Td>Total</Td>
            <Td colSpan={2}>
              {formatCurrency(data.reduce((acc, val) => acc + val.amount, 0))}
            </Td>
          </Tr>
        </Tbody>
      </Table>
    </TableContainer>
  )
}

export default RecurringTransactionsTable
