import React from 'react'
import {
  CardContent,
  Card,
  CardHeader,
  Paper
} from '@material-ui/core'
import RecurringContainer from './RecurringContainer'

const RecurringCard: React.FC = () => {
  return (
    <Paper>
      <Card variant="outlined">
        <CardHeader title="Recurring expenses and income" />
        <CardContent>
          <RecurringContainer />
        </CardContent>
      </Card>
    </Paper>
  )
}

export default RecurringCard
