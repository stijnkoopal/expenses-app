import { Grid } from '@material-ui/core'
import useSuspendedQuery from 'api/use-suspended-query'
import LoaderTable from 'components/LoaderTable'
import { parseISO } from 'date-fns'
import gql from 'graphql-tag'
import React, { Suspense } from 'react'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'
import RecurringTransactionsTable, { RecurringTransaction as RecurringTransactionInTable, TransactionDirection } from './RecurringTransactionsTable'

interface TransactionParty {
  name?: string
  iban?: string
}

interface RecurringTransaction {
  payee: TransactionParty
  payer: TransactionParty
  startDate: string
  endDate: string
  frequency: string
  amount: number
  monetaryAccount: {
    id: string
  }
}

interface RecurringTransactionsResponse {
  recurringTransactions: RecurringTransaction[]
}

// TODO: we can minimize this graphql query
const RECURRING_TRANSACTIONS_QUERY = gql`
  query RecurringTransactions(
    $monetaryAccounts: [MonetaryAccountConfiguration!]!
    $direction: TransactionDirection!
  ) {
    recurringTransactions(
      monetaryAccounts: $monetaryAccounts
      direction: $direction
    ) {
      startDate
      endDate
      frequency
      amount
      payee {
        iban
        name
      }
      payer {
        iban
        name
      }
      monetaryAccount {
        id
      }
    }
  }
`

const RecuringExpenes: React.FC = () => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()
  const { data, error } = useSuspendedQuery<RecurringTransactionsResponse>(
    RECURRING_TRANSACTIONS_QUERY,
    {
      variables: {
        direction: 'Outbound',
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  const mappedData: RecurringTransactionInTable[] = data.recurringTransactions.map(
    tx => ({
      payee: tx.payee,
      payer: tx.payer,
      amount: tx.amount,
      monetaryAccountId: tx.monetaryAccount.id,
      start: parseISO(tx.startDate),
      end: tx.endDate ? parseISO(tx.endDate) : undefined,
      frequency: tx.frequency
    })
  ).sort((a, b) => (a.payee?.name || '') < (b.payee?.name || '') ? -1 : 1)

  return <RecurringTransactionsTable direction={TransactionDirection.Outbound} data={mappedData} />
}

const RecuringIncome: React.FC = () => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()
  const { data, error } = useSuspendedQuery<RecurringTransactionsResponse>(
    RECURRING_TRANSACTIONS_QUERY,
    {
      variables: {
        direction: 'Inbound',
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  const mappedData: RecurringTransactionInTable[] = data.recurringTransactions.map(
    tx => ({
      payee: tx.payee,
      payer: tx.payer,
      amount: tx.amount,
      monetaryAccountId: tx.monetaryAccount.id,
      start: parseISO(tx.startDate),
      end: tx.endDate ? parseISO(tx.endDate) : undefined,
      frequency: tx.frequency
    })
  )

  return <RecurringTransactionsTable direction={TransactionDirection.Inbound} data={mappedData} />
}

const RecurringContainer: React.FC = () => {
  return (
    <Suspense fallback={<LoaderTable />}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <RecuringExpenes />
        </Grid>
        <Grid item xs={12} md={6}>
          <RecuringIncome />
        </Grid>
      </Grid>
    </Suspense>
  )
}

export default RecurringContainer
