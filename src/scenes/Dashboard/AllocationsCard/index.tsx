import React, { useState, useCallback } from 'react'
import { CardContent, Card, CardHeader, IconButton, Paper } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import LabelTransactionDialog from './LabelTransactionDialog'

const AllocationsCard: React.FC = () => {
  const [open, setOpen] = useState(false)

  const closeDialog = useCallback(() => setOpen(false), [setOpen])
  const openDialog = useCallback(() => setOpen(true), [setOpen])

  return (
    <Paper>
      <Card variant="outlined">
        <LabelTransactionDialog open={open} onClose={closeDialog} />
        <CardHeader
          title="Allocations"
          action={
            <IconButton
              aria-label="settings"
              onClick={openDialog}
              aria-controls="monetary-accounts-more-menu"
              aria-haspopup="true"
            >
              <MoreVertIcon />
            </IconButton>
          }
        />
        <CardContent></CardContent>
      </Card>
    </Paper>
  )
}

export default AllocationsCard
