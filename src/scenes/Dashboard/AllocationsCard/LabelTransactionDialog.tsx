import React from "react"
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "@material-ui/core"

const LabelTransactionDialog: React.FC<{open: boolean, onClose: () => void}> = ({open, onClose}) => {
    return (
      <Dialog
        fullWidth={true}
        maxWidth="md"
        open={open}
        aria-labelledby="label-transaction-dialog-title"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
      >
        <DialogTitle id="label-transaction-dialog-title">
          {'Label transaction'}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>Test</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="secondary">
            Close
          </Button>
          <Button autoFocus color="primary">
            Save &amp; Next
          </Button>
        </DialogActions>
      </Dialog>
    )
}

export default LabelTransactionDialog
