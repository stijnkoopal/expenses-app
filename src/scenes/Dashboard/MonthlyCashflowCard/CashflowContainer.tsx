import useSuspendedQuery from 'api/use-suspended-query'
import {
  addMonths,
  endOfMonth,
  format,
  parseISO,
  startOfMonth,
  subMonths,
} from 'date-fns'
import gql from 'graphql-tag'
import React, { Suspense } from 'react'
import LoaderXYChart from '../../../components/LoaderXYChart'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'
import MonthlyCashflowChart from './MonthlyCashflowChart'

interface MonthlyCashflow {
  monthStart: string
  inbound: number
  outbound: number
  recurringOutbound: number
  recurringInbound: number
}

interface MonthlyCashflowResponse {
  monthlyCashflow: MonthlyCashflow[]
}

const MONTHLY_CASHFLOW_QUERY = gql`
  query MonthlyCashflow(
    $monetaryAccounts: [MonetaryAccountConfiguration!]!
    $startDate: LocalDate!
    $endDate: LocalDate!
  ) {
    monthlyCashflow(
      monetaryAccounts: $monetaryAccounts
      startDate: $startDate
      endDate: $endDate
    ) {
      monthStart
      inbound
      outbound
      recurringOutbound
      recurringInbound
    }
  }
`

const prepareCashflowData = (
  data: {
    monthStart: string
    inbound: number
    outbound: number
    recurringOutbound: number
    recurringInbound: number
  }[]
) =>
  data.map(
    ({
      monthStart,
      inbound,
      outbound,
      recurringOutbound,
      recurringInbound,
    }) => ({
      month: startOfMonth(parseISO(monthStart)),
      inbound,
      outbound,
      recurringOutbound,
      recurringInbound,
    })
  )

const Cashflow: React.FC<{
  periodStart: Date
  periodEnd: Date
}> = ({ periodStart, periodEnd }) => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()

  const { data, error } = useSuspendedQuery<MonthlyCashflowResponse>(
    MONTHLY_CASHFLOW_QUERY,
    {
      variables: {
        startDate: format(periodStart, 'yyyy-MM-dd'),
        endDate: format(periodEnd, 'yyyy-MM-dd'),
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  return (
    <MonthlyCashflowChart
      data={prepareCashflowData(data.monthlyCashflow)}
      periodStart={periodStart}
      periodEnd={periodEnd}
    />
  )
}

const CashflowContainer: React.FC = () => {
  const periodMonths = 12
  const periodStart = subMonths(startOfMonth(new Date()), periodMonths)
  const periodEnd = addMonths(endOfMonth(periodStart), periodMonths)

  return (
    <Suspense fallback={<LoaderXYChart />}>
      <Cashflow periodStart={periodStart} periodEnd={periodEnd} />
    </Suspense>
  )
}

export default CashflowContainer
