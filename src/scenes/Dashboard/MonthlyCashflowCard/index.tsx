import React, { useCallback, Suspense } from 'react'
import {
  CardContent,
  Card,
  CardHeader,
  Menu,
  MenuItem,
  IconButton,
  ListItemIcon,
  Typography,
  Paper
} from '@material-ui/core'
import CashflowContainer from './CashflowContainer'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import RefreshIcon from '@material-ui/icons/Refresh'
import useMenuAnchor from '../use-menu-anchor'
import useApi from 'api/use-api'
import LoaderMenuItem from 'components/LoaderMenuItem'

const RefreshMenuItem: React.FC<{ onClick: () => void }> = ({onClick}) => {
  const { fetch } = useApi()
  const handleRefresh = useCallback(() => {
    fetch('/util/refresh-monetary-accounts', {
      method: 'POST',
    })
    onClick()
  }, [fetch, onClick])

  return (
    <MenuItem onClick={handleRefresh}>
      <ListItemIcon>
        <RefreshIcon fontSize="small" />
      </ListItemIcon>
      <Typography variant="inherit">Refresh</Typography>
    </MenuItem>
  )
}

const MonthlyCashflowCard: React.FC = () => {  
  const [anchor, handleMenuOpen, handleMenuClose] = useMenuAnchor()

  return (
    <Paper>
      <Card variant="outlined">
        <CardHeader
          title="Monthly cashflow"
          action={
            <IconButton
              aria-label="settings"
              onClick={handleMenuOpen}
              aria-controls="monthly-cashflow-more-menu"
              aria-haspopup="true"
            >
              <MoreVertIcon />
            </IconButton>
          }
        />
        <Menu
          id="monthly-cashflow-more-menu"
          anchorEl={anchor}
          keepMounted
          open={Boolean(anchor)}
          onClose={handleMenuClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          getContentAnchorEl={null}
        >
          <Suspense fallback={<LoaderMenuItem />}>
            <RefreshMenuItem onClick={handleMenuClose} />
          </Suspense>
        </Menu>
        <CardContent>
          <CashflowContainer />
        </CardContent>
      </Card>
    </Paper>
  )
}

export default MonthlyCashflowCard
