import {
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from '@material-ui/core'
import { PointTooltipProps, ResponsiveLine, Serie } from '@nivo/line'
import NivoChartsContainer from 'components/NivoChartsContainer'
import { addMonths, format, fromUnixTime, getMonth, isAfter } from 'date-fns'
import React from 'react'
import { formatCurrency } from 'scenes/format-currency'
import { negative, positive } from 'themes/chart-colors'

export interface MonthlyCashflowDataPoint {
  month: Date
  inbound?: number
  outbound?: number
  recurringOutbound?: number
  recurringInbound?: number
}

interface MonthCashflowWithSavingsRateDataPoint
  extends MonthlyCashflowDataPoint {
  savingsRate?: number
}

const addSavingsRate = (
  data: MonthlyCashflowDataPoint[]
): MonthCashflowWithSavingsRateDataPoint[] => {
  const calcSavingsRate = (inbound: number, outbound: number) =>
    inbound > 0 ? Math.round(((inbound - outbound) / inbound) * 100) : 0

  return data.map((point) => ({
    ...point,
    savingsRate: calcSavingsRate(point.inbound || 0, point.outbound || 0),
  }))
}

const appendEmptyMonths = (
  data: MonthlyCashflowDataPoint[],
  dateStart: Date,
  dateEnd: Date
): MonthlyCashflowDataPoint[] => {
  const castedData = data.map((d) => ({ ...d, month: d.month.getTime() }))
  if (isAfter(dateStart, dateEnd)) {
    return data
  }

  const monthsInBetween: Date[] = []
  let dateChange = dateStart

  while (
    isAfter(dateEnd, dateChange) ||
    getMonth(dateChange) === getMonth(dateEnd)
  ) {
    monthsInBetween.push(dateChange)
    dateChange = addMonths(dateChange, 1)
  }

  const result = [...castedData]
  monthsInBetween.forEach((month) => {
    if (!castedData.map(({ month }) => month).includes(month.getTime())) {
      result.push({
        month: month.getTime(),
      })
    }
  })

  return result
    .sort((a, b) => (isAfter(a.month, b.month) ? 1 : -1))
    .map((point) => ({ ...point, month: fromUnixTime(point.month / 1000) }))
}

const adaptToNivoSeries = (
  data: MonthCashflowWithSavingsRateDataPoint[]
): Serie[] => {
  const inboundSerie: Serie = {
    id: 'inbound',
    color: positive,
    data: data.map((point) => ({
      x: format(point.month, 'y-MM-dd'),
      y: point.inbound || 0,
    })),
  }

  const outboundSerie: Serie = {
    id: 'outbound',
    color: negative,
    data: data.map((point) => ({
      x: format(point.month, 'y-MM-dd'),
      y: point.outbound || 0,
    })),
  }

  const recurringOutboundSerie: Serie = {
    id: 'recurringOutbound',
    color: '#a03a56',
    data: data.map((point) => ({
      x: format(point.month, 'y-MM-dd'),
      y: point.recurringOutbound || 0,
    })),
  }

  // const recurringInboundSerie: Serie = {
  //   id: 'recurringInbound',
  //   color: positive,
  //   data: data.map((point) => ({
  //     x: format(point.month, 'y-MM-dd'),
  //     y: point.recurringInbound || 0,
  //   })),
  // }

  const savingsRate: Serie = {
    id: 'savings-rate',
    color: 'grey',
    data: [],
  }

  return [inboundSerie, outboundSerie, recurringOutboundSerie, savingsRate]
}

// TODO: doesnt work: https://github.com/plouc/nivo/issues/251
const Tooltip: React.FC<PointTooltipProps> = ({ point }) => {
  console.log(point)

  return (
    <>
      <strong>testd</strong>
      <List dense>
        <ListItem button>
          <ListItemText primary={'x'} />
          <ListItemSecondaryAction></ListItemSecondaryAction>
        </ListItem>
      </List>
    </>
  )
}

// TODO: add reference line for years
// TODO?: add savings rate
const MonthlyCashflowChart: React.FC<{
  data?: MonthlyCashflowDataPoint[]
  periodStart: Date
  periodEnd: Date
}> = ({ data = [], periodStart, periodEnd }) => {
  const correctedData: Serie[] = adaptToNivoSeries(
    appendEmptyMonths(addSavingsRate(data), periodStart, periodEnd)
  )

  return (
    <NivoChartsContainer>
      <ResponsiveLine
        data={correctedData}
        margin={{ top: 10, right: 20, bottom: 50, left: 50 }}
        markers={[]}
        xScale={{
          type: 'time',
          format: '%Y-%m-%d',
          precision: 'month',
        }}
        xFormat="time:%b %y"
        yScale={{
          type: 'linear',
          min: 'auto',
          max: 'auto',
          stacked: false,
          reverse: false,
        }}
        axisLeft={{
          format: (value) => formatCurrency(Number(value)),
        }}
        yFormat={(value) => formatCurrency(Number(value))}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          format: '%b %y',
          tickValues: 'every 1 month',
          legendOffset: 36,
        }}
        enableSlices="x"
        colors={{ datum: 'color' }}
        pointSize={10}
        pointColor={{ theme: 'background' }}
        pointBorderWidth={2}
        pointBorderColor={{ from: 'serieColor' }}
        pointLabel="y"
        pointLabelYOffset={-12}
        useMesh={true}
        tooltip={({ point }) => <Tooltip point={point} />}
        legends={[
          {
            anchor: 'bottom',
            direction: 'row',
            justify: false,
            translateX: 0,
            translateY: 50,
            itemsSpacing: 0,
            itemDirection: 'left-to-right',
            itemWidth: 100,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: 'circle',
            symbolBorderColor: 'rgba(0, 0, 0, .5)',
            effects: [
              {
                on: 'hover',
                style: {
                  itemBackground: 'rgba(0, 0, 0, .03)',
                  itemOpacity: 1,
                },
              },
            ],
          },
        ]}
      />
    </NivoChartsContainer>
  )
}

export default MonthlyCashflowChart
