import useSuspendedQuery from 'api/use-suspended-query'
import gql from 'graphql-tag'
import { useGlobal } from 'reactn'

interface MonetaryAccountInCluster {
  monetaryAccount: {
    id: string
  }
  joint: boolean
}

enum MonetaryAccountClusterType {
  All,
  UserDefined,
}

export interface MonetaryAccountCluster {
  id: string
  name: string
  clusterType: MonetaryAccountClusterType
  selected: boolean
  monetaryAccounts: MonetaryAccountInCluster[]
}

interface MonetaryAccountClustersResponse {
  monetaryAccountClusters: MonetaryAccountCluster[]
}

const MONETARY_ACCOUNT_CLUSTERS_QUERY = gql`
  query MonetaryAccountClusters {
    monetaryAccountClusters {
      id
      name
      clusterType
      selected
      monetaryAccounts {
        monetaryAccount {
          id
        }
        joint
      }
    }
  }
`

const useFetchMonetaryAccountClusters = () => {
  const { data, error } = useSuspendedQuery<MonetaryAccountClustersResponse>(
    MONETARY_ACCOUNT_CLUSTERS_QUERY
  )

  if (error || !data) {
    throw error
  }

  return data.monetaryAccountClusters
}

let monetaryAccountClusters: MonetaryAccountCluster[] | undefined = undefined

export const useMonetaryAccountClusters = (): MonetaryAccountCluster[] => {
  const fetchedClusters = useFetchMonetaryAccountClusters()

  if (monetaryAccountClusters) {
    return monetaryAccountClusters
  }

  monetaryAccountClusters = fetchedClusters
  return monetaryAccountClusters
}

export const useSelectedMonetaryAccountClusterOrSuspend = (): MonetaryAccountCluster | undefined => {
  const [selectedMonetaryAccountCluster, setX] = useGlobal<{
    selectedMonetaryAccountCluster: MonetaryAccountCluster | undefined
  }>('selectedMonetaryAccountCluster')

  const clusters = useMonetaryAccountClusters()

  if (!selectedMonetaryAccountCluster && clusters.length > 0) {
    const selectedClusterFromBackend =
      clusters.find((cluster) => cluster.selected) ||
      clusters.find(
        (cluster) => cluster.clusterType === MonetaryAccountClusterType.All
      ) ||
      clusters[0]
    setX(selectedClusterFromBackend)
    return selectedClusterFromBackend
  }

  return selectedMonetaryAccountCluster
}
