import React, { useCallback, useState } from 'react'

const useMenuAnchor: () => [
  HTMLElement | null,
  (event: React.MouseEvent<HTMLElement, MouseEvent>) => void,
  () => void
] = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

  const handleMenuOpen = useCallback(
    (event: React.MouseEvent<HTMLElement>) => setAnchorEl(event.currentTarget),
    []
  )

  const handleMenuClose = useCallback(() => setAnchorEl(null), [])

  return [anchorEl, handleMenuOpen, handleMenuClose]
}

export default useMenuAnchor
