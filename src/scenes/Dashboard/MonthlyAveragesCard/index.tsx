import { Box, Card, CardContent, CardHeader, Grid, LinearProgress, Paper, withStyles } from '@material-ui/core'
import useSuspendedQuery from 'api/use-suspended-query'
import { addMonths, format, startOfMonth, subMonths } from 'date-fns'
import gql from 'graphql-tag'
import React, { Suspense } from 'react'
import { formatCurrency } from 'scenes/format-currency'
import { negative, positive } from 'themes/chart-colors'
import { useSelectedMonetaryAccountClusterOrSuspend } from '../use-monetary-account-clusters'

interface MonthlyCashflowAverages {
  inbound: number
  outbound: number
}

interface RecurringSummed {
  inbound: number
  outbound: number
}

interface MonthlyAveragesResponse {
  monthlyCashflowAverages: MonthlyCashflowAverages
  recurringSummed: RecurringSummed
}

const MONTHLY_AVERAGES_QUERY = gql`
  query MonthlyAverages(
    $monetaryAccounts: [MonetaryAccountConfiguration!]!
    $startDate: LocalDate!
    $endDate: LocalDate!
  ) {
    monthlyCashflowAverages(
      monetaryAccounts: $monetaryAccounts
      startDate: $startDate
      endDate: $endDate
    ) {
      inbound
      outbound
    }

    recurringSummed(monetaryAccounts: $monetaryAccounts) {
      inbound
      outbound
    }
  }
`

const InboundBar = withStyles({
  colorPrimary: {
    backgroundColor: positive,
  },
  barColorPrimary: {
    backgroundColor: '#07543d',
  },
})(LinearProgress)

const OutboundBar = withStyles({
  colorPrimary: {
    background: negative,
  },
  barColorPrimary: {
    backgroundColor: '#a03a56',
  },
})(LinearProgress)

const BarContainer = (props: any) => (
  <Box display="flex" flexDirection="row" alignItems="center" {...props}>
    {props.children}
  </Box>
)

const Averages: React.FC<{
  inbound: number
  outbound: number
  outboundRecurring: number
  inboundRecurring: number
}> = ({ inbound, outbound, outboundRecurring, inboundRecurring }) => {
  const gaining = inbound >= -outbound
  return (
    <Grid container spacing={2}>
      <Grid item xs={2} md={3} lg={2}>
        <Box display="flex" flexDirection="column">
          <Box fontWeight="fontWeightBold" fontSize={'1.75rem'}>
            {formatCurrency(inbound + outbound)}
          </Box>
          <Box fontSize={'1rem'}>{gaining ? 'Gaining' : 'Loosing'}</Box>
        </Box>
      </Grid>
      <Grid item xs={10} md={9} lg={10}>
        <BarContainer>
          <InboundBar
            value={(inboundRecurring / inbound) * 100}
            style={{ width: `${gaining ? 100 : (inbound / -outbound) * 100}%` }}
            variant="determinate"
          />
          <Box ml={1} color={positive}>
            Income {formatCurrency(inbound)}
          </Box>
        </BarContainer>
        <BarContainer>
          <OutboundBar
            value={(outboundRecurring / outbound) * 100}
            style={{ width: `${gaining ? (-outbound / inbound) * 100 : 100}%` }}
            variant="determinate"
          />
          <Box ml={1} color={negative}>
            Expenses {formatCurrency(outbound)}
          </Box>
        </BarContainer>
      </Grid>
    </Grid>
  )
}

const AveragesSuspending: React.FC<{
  periodStart: Date
  periodEnd: Date
}> = ({ periodStart, periodEnd }) => {
  const cluster = useSelectedMonetaryAccountClusterOrSuspend()

  const { data, error } = useSuspendedQuery<MonthlyAveragesResponse>(
    MONTHLY_AVERAGES_QUERY,
    {
      variables: {
        startDate: format(periodStart, 'yyyy-MM-dd'),
        endDate: format(periodEnd, 'yyyy-MM-dd'),
        // TODO: improve the ?. here, we shouldnt call backend
        monetaryAccounts: (cluster?.monetaryAccounts || []).map((config) => ({
          monetaryAccountId: config.monetaryAccount.id,
          joint: config.joint,
        })),
      },
    }
  )

  if (error || !data) {
    return <div>ERROR!</div>
  }

  return (
    <Averages
      inbound={data.monthlyCashflowAverages.inbound}
      outbound={data.monthlyCashflowAverages.outbound}
      outboundRecurring={data.recurringSummed.outbound}
      inboundRecurring={data.recurringSummed.inbound}
    />
  )
}

const MonthlyAveragesCard: React.FC = () => {
  const periodMonths = 12
  const periodStart = subMonths(startOfMonth(new Date()), periodMonths)
  const periodEnd = addMonths(periodStart, periodMonths)

  return (
    <Paper>
      <Card variant="outlined">
        <CardHeader title="Monthly averages" />
        <CardContent>
          <Suspense fallback={'loading...'}>
            <AveragesSuspending
              periodStart={periodStart}
              periodEnd={periodEnd}
            />
          </Suspense>
        </CardContent>
      </Card>
    </Paper>
  )
}

export default MonthlyAveragesCard
