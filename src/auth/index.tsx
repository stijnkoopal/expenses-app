import * as auth0 from './auth0';
import { myOrigin } from '../config'

export type Permission = string;

export interface User {
  id: string
  name: string
  givenName: string
  familyName: string
  email: string
  picture: string
  permissions: Permission[]
}

const afterLoginUrl = '/dashboard'
const afterLogoutUrl = `${myOrigin}/`

const extractUser = (auth0User?: auth0.User): User | undefined => {
  if (!auth0User) {
    return undefined
  }

  return ({
    name: auth0User.name,
    givenName: auth0User.given_name,
    familyName: auth0User.family_name,
    email: auth0User.email,
    picture: auth0User.picture,
    id: auth0User.sub,
    permissions: auth0User.authorization?.permissions || []
  })
}

export const login = auth0.loginWithRedirect
export const logout = () => auth0.logout({ returnTo: afterLogoutUrl })
export const isAuthenticated = auth0.authenticatedResource
export const token = auth0.tokenResource
export const getUser = () => extractUser(auth0.userResource.read())

auth0.handleLoginRedirect(afterLoginUrl)
