import React from 'react'
import { Route } from 'react-router-dom'
import * as auth from '.'
import { AuthenticationRequiredError } from './errors'

class AuthenticatedRequiredErrorBoundary extends React.Component<{}, { isRedirecting: boolean }> {
  constructor(props: Object) {
    super(props)
    this.state = { isRedirecting: false }
  }

  componentDidCatch(error: any, errorInfo: any) {
    if (error instanceof AuthenticationRequiredError) {
      this.setState({isRedirecting: true});
      auth.login()
    } else {
      throw error;
    }
  }

  render() {
    if (this.state.isRedirecting) {
      return null
    }

    return this.props.children
  }
}

const PrivateRoute: React.FC<{ Component: React.ElementType; path: string }> = ({
  Component,
  path,
  ...rest
}) => {
  const render = (props: Object) => (
    <AuthenticatedRequiredErrorBoundary>
      <Component {...props} />
    </AuthenticatedRequiredErrorBoundary>
  )

  return <Route path={path} render={render} {...rest} />
}

export default PrivateRoute
