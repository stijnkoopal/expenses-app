import { Permission } from '.'

const Can: React.FC<{
  yes?: () => React.ReactElement,
  no?: () => React.ReactElement,
  perform: string,
  permissions: Permission[],
}> = ({yes = () => null, no = () => null, perform, permissions}) => {
  return permissions.includes(perform)
    ? yes() 
    : no()
}

export default Can
