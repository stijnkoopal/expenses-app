import { HttpLink, InMemoryCache, NormalizedCacheObject } from 'apollo-boost'
import { ApolloClient } from 'apollo-client'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { ServerError } from 'apollo-link-http-common'
import * as auth from '.'
import { apiOrigin } from '../config'

let client: ApolloClient<NormalizedCacheObject>

const tokenResource = auth.token

const useApolloClient: () => ApolloClient<NormalizedCacheObject> = () => {
  if (client) {
    return client
  }
  
  const token = tokenResource.read()

  const withToken = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: `Bearer ${token}`,
        'Csrf-Token': 'nocheck',
      },
    }
  })

  const resetToken = onError(({ networkError }) => {
    if (
      networkError &&
      networkError.name === 'ServerError' &&
      (networkError as ServerError).statusCode === 401
    ) {
      auth.logout()
    }
  })

  const authFlowLink = withToken.concat(resetToken)

  const httpLink = new HttpLink({
    uri: `${apiOrigin}/graphql`,
  })

  const result = new ApolloClient({
    link: authFlowLink.concat(httpLink),
    cache: new InMemoryCache({
      freezeResults: true,
    }),
    assumeImmutableResults: true,
  })

  client = result
  return client
}

export default useApolloClient
