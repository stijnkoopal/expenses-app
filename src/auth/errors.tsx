export class AuthenticationRequiredError {
    constructor(public delegate?: any) {
    }
}

export class UnableToFetchUser {
  constructor(public delegate?: any) {}
}
