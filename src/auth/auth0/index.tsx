import createAuth0Client, { LogoutOptions } from '@auth0/auth0-spa-js'
import config from './config.json'
import suspend from '../../suspend'
import { AuthenticationRequiredError, UnableToFetchUser } from '../errors'

export type RedirectCallback = (appState?: { targetUrl?: string }) => void

export interface User {
  name: string
  given_name: string
  family_name: string
  email: string
  picture: string
  sub: string
  authorization?: {
    permissions: string[]
  }
  geoip?: {
    country_code: string
    country_code3: string
    country_name: string
    city_name: string
    latitude: number
    longitude: number
    time_zone: string
    continent_code: string
  }
}

const withAudienceProperties = (user: any, audience?: string): User => {
  return Object.keys(user).reduce((acc, val) => {
    if (audience && val.startsWith(audience)) {
      return {
        ...acc,
        [val.replace(audience + '/', '').replace(audience, '')]: user[val],
      }
    }
    return { ...acc, [val]: user[val] }
  }, {}) as any
}

const onRedirectCallback = (loginSuccessUrl: string, appState?: {
  targetUrl?: string
}) => {
  // eslint-disable-next-line no-restricted-globals
  location.replace(
    appState && appState.targetUrl ? appState.targetUrl : loginSuccessUrl
  )
}

const auth0ClientPromise = createAuth0Client({
  domain: config.domain,
  client_id: config.clientId,
  redirect_uri: window.location.origin,
  audience: config.audience,
} as any)

export const authenticatedResource = suspend(
  auth0ClientPromise.then(client => client.isAuthenticated())
)

export const userResource = suspend(
  auth0ClientPromise
    .then(client => client.getUser())
    .then(user => {
      if (!user) {
        throw new UnableToFetchUser()
      }
      return withAudienceProperties(user, config.audience)
    })
)

export const tokenResource = suspend(
  auth0ClientPromise
    .then(client => client.getTokenSilently())
    .catch(err => {
      if (err.error === 'login_required') {
        throw new AuthenticationRequiredError(err)
      }
      throw err
    })
)

export const tokenClaimsResource = suspend(
  auth0ClientPromise.then(client => client.getIdTokenClaims())
)

export const logout = (options?: LogoutOptions) =>
  suspend(auth0ClientPromise.then(client => client.logout(options)))

export const loginWithRedirect = () =>
  auth0ClientPromise.then(client => client.loginWithRedirect())

export const handleLoginRedirect = (loginSuccessUrl: string) => {
  if (
    window.location.search.includes('code=') &&
    window.location.pathname === '/'
  ) {
    auth0ClientPromise
      .then(client => client.handleRedirectCallback())
      .then(loginResult =>
        onRedirectCallback(loginSuccessUrl, loginResult.appState)
      )
  }
}
