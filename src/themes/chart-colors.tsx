import {
  teal, 
  red,
  blue,
  pink,
  green,
  purple,
  indigo,
  lime,
  amber,
  blueGrey,
  yellow,
  brown,
  grey
} from '@material-ui/core/colors'

const chartColors = [
  green[300],
  red[300],
  blue[300],
  pink[300],
  teal[300],
  indigo[300],
  purple[300],
  lime[300],
  amber[300],
  blueGrey[300],
  yellow[300],
  brown[300]
]

export const positive = '#07815c'
export const negative = '#cd3d64'
export const neutral = grey[500]

export default chartColors
