import React from 'react'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
  createMuiTheme,
  responsiveFontSizes, 
  ThemeProvider as MuiThemeProvider,
} from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

const ThemeProvider: React.FC<{useUserPreferences?: Boolean}> = ({children, useUserPreferences = false}) => {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      responsiveFontSizes(
        createMuiTheme({
        palette: {
          type: useUserPreferences && prefersDarkMode ? 'dark' : 'light',
          primary: {
            main: '#3f51b5',
          },
          background: {
            paper: '#ffffff',
            default: '#f4f4f4',
          },
          text: {
            primary: '#868e96',
          },
        },
        typography: {
          fontFamily: ['Nunito Sans,sans-serif'].join(', '),
        },
      })
    ),
    [useUserPreferences, prefersDarkMode]
  )

  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </MuiThemeProvider>
  )
}

export default ThemeProvider
