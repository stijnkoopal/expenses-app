import React from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import PrivateRoute from 'auth/PrivateRoute'
import ThemeProvider from 'themes/ThemeProvider'
import Connections from 'scenes/Connections'
import Dashboard from 'scenes/Dashboard'
import Landing from 'scenes/Landing'
import Admin from 'scenes/Admin'
import history from './my-history'

const App: React.FC = () => {
  return (
    <ThemeProvider>
      <Router history={history}>
          <Switch>
            <PrivateRoute
              path="/connections/authorize/:connector"
              Component={Connections.Authorize.Scene}
            />
            <PrivateRoute path="/connections" Component={Connections.Scene} />
            <PrivateRoute path="/dashboard" Component={Dashboard.Scene} />
            <PrivateRoute path="/admin" Component={Admin.Scene} />
            <Route path="/" exact>
              <Landing.Scene />
            </Route>
          </Switch>
      </Router>
    </ThemeProvider>
  )
}

export default App
