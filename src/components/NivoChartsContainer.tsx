import React from 'react'

const NivoChartsContainer: React.FC = ({ children }) => {
  return (
    <div
      style={{
        position: 'relative',
        minHeight: '500px',
        width: '100%',
        height: '100%',
      }}
    >
      <div
        style={{
          position: 'absolute',
          height: '100%',
          width: '100%',
          zIndex: 100,
        }}
      >
        {children}
      </div>
    </div>
  )
}

export default NivoChartsContainer
