import React from 'react'
import { Box } from "@material-ui/core"

const Iban: React.FC<{iban: string}> = ({iban}) => {
    return (
        <Box>{iban}</Box>
    )
}

export default Iban
